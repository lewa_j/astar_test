
#include <stdio.h>

#include "AStar.h"

int main()
{
	AStarNav nav;

	LoadObj("waypoints.obj", nav);

	Vec3 startPos = Vec3(0, 0, 0);
	Vec3 goalPos = Vec3(1.5, 1.5, 0);
	int startNode = nav.getNearestNode(startPos);
	int goalNode = nav.getNearestNode(goalPos);

	float length = 0;
	auto path = nav.getPath(startNode, goalNode, length);
	printf("Path nodes count %zu, length %g\n", path.size(), length);

	for (auto n : path)
	{
		Vec3 v = nav.getNodePos(n);
		printf("%d (%g, %g, %g)\n",n,v.x,v.y,v.z);
	}
}
