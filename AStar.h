#pragma once
//#define ASTAR_UNIGINE

#ifdef ASTAR_UNIGINE
#include <UnigineMathLib.h>
using Unigine::Math::Vec3;
#else
#include "Math.h"
#endif

#include <vector>

class ANode;

class ANodeEdge
{
public:
	ANode* node;
	float distance;
};

class ANode
{
public:
	ANode() : position(), minDistance(-1), heuristic(0), prevNode(nullptr) {}
	ANode(Vec3 p, std::vector<ANode*> e)
		: position(p), minDistance(-1), heuristic(0), prevNode(nullptr)
	{
		edges.resize(e.size());
		for (size_t i = 0; i < e.size(); i++)
			edges[i].node = e[i];
	}

	Vec3 position;
	std::vector<ANodeEdge> edges;

	void calcDistances()
	{
		for (size_t i = 0; i < edges.size(); i++)
		{
			edges[i].distance = (position - edges[i].node->position).length();
		}
	}

	float minDistance;//-1 if unvisited
	float heuristic;
	ANode* prevNode;
};

class AStarNav
{
public:
	std::vector<ANode> nodes;

	void initEdges();
	int getNearestNode(Vec3 pos);
	Vec3 getNodePos(int id);

	std::vector<int> getPath(int start, int end, float& outLength);
};

void LoadObj(const char* path, AStarNav& nav);
