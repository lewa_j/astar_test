#include "AStar.h"

#include <stdio.h>
#include <queue>

#ifdef ASTAR_UNIGINE
#include "UnigineVisualizer.h"

using Unigine::Math::Scalar;
using Unigine::Math::Consts::EPS;

#define LOG Unigine::Log::message
#define LOG_WARN Unigine::Log::warning
#define ASSERT UNIGINE_ASSERT
#else
#define LOG printf
#define LOG_WARN printf
#define ASSERT()
#endif

void AStarNav::initEdges()
{
	for (auto& n : nodes) {
		n.calcDistances();
	}
}

int AStarNav::getNearestNode(Vec3 pos)
{
	ASSERT(nodes.size());
	int nearestID = 0;
	Scalar nearestDist2 = (pos - nodes[0].position).length2();

	for (size_t i = 1; i < nodes.size(); i++) {
		Scalar newDist2 = (pos - nodes[i].position).length2();
		if (newDist2 < nearestDist2) {
			nearestDist2 = newDist2;
			nearestID = i;
		}
	}
	return nearestID;
}

Vec3 AStarNav::getNodePos(int id)
{
	ASSERT(id >= 0 && id < nodes.size());
	return nodes[id].position;
}

//returns vector of node indices representing shortest path from start to end
//if no path found, empty vector is returned
std::vector<int> AStarNav::getPath(int start, int end, float& outLength)
{
	if (start == end) {
		outLength = 0;
		return {end};
	}

	//mark all nodes as unvisited
	for (auto n : nodes)
		n.minDistance = -1;

	auto compare = [](ANode* l, ANode* r) {return l->minDistance + l->heuristic > r->minDistance + r->heuristic; };
	std::priority_queue <ANode*, std::vector<ANode*>, decltype(compare)> frontier(compare);

	nodes[start].minDistance = 0;
	nodes[start].heuristic = 0;
	nodes[start].prevNode = nullptr;
	frontier.push(&nodes[start]);

	int totalIterations = 0;

	while (!frontier.empty()) {
		totalIterations++;
		ANode* n = frontier.top();
		frontier.pop();
#if defined( ASTAR_UNIGINE ) && defined ( DEBUG )
		if(n->prevNode)
			Unigine::Visualizer::renderVector(n->prevNode->position, n->position, { 0.5,0,0,0.4 }, 0.25f, false, 60);
#endif
		//we're done. early exit
		if (n == &nodes[end])
			break;

		for (auto& e : n->edges) {
			float newDistance = n->minDistance + e.distance;
			//add node if unvisited or we found new shortest path
			if (e.node->minDistance < 0 || newDistance < e.node->minDistance - EPS)
			{
				e.node->minDistance = newDistance;
				e.node->heuristic = (e.node->position - nodes[end].position).length();
				e.node->prevNode = n;
				frontier.push(e.node);
			}
		}
	}
#ifdef DEBUG
	LOG("total iterations %d\n", totalIterations);
#endif
	if (nodes[end].minDistance < 0)
	{
		LOG_WARN("No path found\n");
		return {};
	}

	std::vector<int> path;

	{//reconstruct path
		ANode* n = &nodes[end];
		int nodesCount = 1;
		while (n != &nodes[start]) {
			n = n->prevNode;
			nodesCount++;
		}

		path.resize(nodesCount);

		n = &nodes[end];
		for (int i = nodesCount - 1; i >= 0; i--) {
			path[i] = n - nodes.data();//index of
			n = n->prevNode;
		}
	}

	outLength = nodes[end].minDistance;
	return path;
}

//Load node graph from wavefront obj
//Uses only vertices and edges
void LoadObj(const char* path, AStarNav& nav)
{
	FILE* f = fopen(path, "r");
	if (!f)
		return;

	char token[1024]{};

	while (true)
	{
		int r = fscanf(f, "%s", token);
		if (r == 0 || feof(f))
			break;

		if (token[0] == 0 || token[0] == '#')
			continue;

		if (!strcmp(token, "v"))
		{
			Vec3 pos;
			fscanf(f, "%f %f %f\n", &pos.x, &pos.y, &pos.z);
			nav.nodes.push_back({ pos,{} });
		}
		else if (!strcmp(token, "l"))
		{
			int edge[2]{};
			fscanf(f, "%d %d\n", edge, edge + 1);
			nav.nodes[edge[0] - 1].edges.push_back({ &nav.nodes[edge[1] - 1] });
			nav.nodes[edge[1] - 1].edges.push_back({ &nav.nodes[edge[0] - 1] });
		}
	}
	fclose(f);

	nav.initEdges();
	LOG("obj loaded %d nodes\n", nav.nodes.size());
}
