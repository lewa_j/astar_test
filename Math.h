#pragma once

#include <math.h>

//Minimal set for this projects
#define EPS 0.0001f
using Scalar = float;

class Vec3
{
public:
	Vec3() : x(0),y(0),z(0) {}
	Vec3(Scalar ax, Scalar ay, Scalar az)
		: x(ax), y(ay), z(az) {}

	Scalar x, y, z;

	Scalar length2()
	{
		return (Scalar)(x * x + y * y + z * z);
	}
	Scalar length()
	{
		return (Scalar)sqrt(x * x + y * y + z * z);
	}
	
};

inline Vec3 operator- (const Vec3 &l, const Vec3 &r)
{
	return Vec3(l.x - r.x, l.y - r.y, l.z - l.z);
}
